class LocksController < ApplicationController
  inherit_resources
  actions :all, :except => [ :new ]
  
  skip_before_filter :authenticate_person!, :only => [:index]
  before_filter :clear_expired_locks, :only => [:index]

  def index
    redirect_with_delay(root_url, 5)
    index!
  end

  def update
    update! do |success, failure|
      success.html { redirect_to collection_url }
    end
  end
  
  def create
    @lock = Lock.new(params[:lock])
    @lock.machine_id = params[:machine_id] if params[:machine_id]
    @lock.person = current_person
    @lock.done_at = 2.hours.from_now
    begin
      create!(:notice => "It's all yours. Happy building.") { locks_path }
    rescue ActiveRecord::RecordNotUnique
      redirect_to(locks_path)
    end
  end
  
  def extend
    lock = Lock.find(params[:id])
    if lock && lock.machine
      lock.extend_time(30)

      respond_to do |wants|
        if lock.save
          flash[:notice] = "Lock extended 30 minutes."
          wants.html { redirect_to(locks_path) }
          wants.xml  { render :xml => lock, :status => :created, :location => lock }
        else
          wants.html { render :action => "index" }
          wants.xml  { render :xml => lock.errors, :status => :unprocessable_entity }
        end
      end
    else
      redirect_to(locks_path)
    end
  end

  private
  
  # clear expired lock for the current location
  def clear_expired_locks
    Lock.expired.clear
  end
end
