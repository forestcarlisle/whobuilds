class PeopleController < ApplicationController
  before_filter :find_person, :only => [:edit, :update, :destroy]

  # GET /person/edit
  def edit
  end

  # PUT /person
  # PUT /person.xml
  def update
    # if resource.update_with_password(params[resource_name])
    #   set_flash_message :notice, :updated
    #   redirect_to after_update_path_for(resource)
    # else
    #   clean_up_passwords(resource)
    #   render_with_scope :edit
    # end
    
    respond_to do |wants|
      if @person.update_with_password(params[:person])
        flash[:notice] = 'You updated your account successfully.'
        wants.html { redirect_to(after_update_path_for(@person)) }
        wants.xml  { head :ok }
      else
        @person.clean_up_passwords if @person.respond_to?(:clean_up_passwords)
        wants.html { render :action => "edit" }
        wants.xml  { render :xml => @person.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /peoples/1
  # DELETE /peoples/1.xml
  def destroy
    @person.destroy

    respond_to do |wants|
      wants.html { redirect_to(root_url) }
      wants.xml  { head :ok }
    end
  end

  private
    def find_person
      @person = current_person
    end

end
