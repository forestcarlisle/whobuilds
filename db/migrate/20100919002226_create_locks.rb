class CreateLocks < ActiveRecord::Migration
  def self.up
    create_table :locks do |t|
      t.references :person
      t.references :machine
      t.datetime :done_at

      t.timestamps
    end
  end

  def self.down
    drop_table :locks
  end
end
