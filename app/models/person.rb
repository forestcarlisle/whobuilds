class Person < ActiveRecord::Base
  has_many :locks, :dependent => :destroy
  has_many :locked_machines, :through => :locks, :class_name => "Machine", :foreign_key => "machine_id"

  devise :database_authenticatable, :token_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable,
         :lockable, :timeoutable, :invitable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :invitation_token
  
  validates :name, :presence => true, :uniqueness => true
  validates :email, :presence => true, :uniqueness => true
  
  after_create :ensure_authentication_token!
end
