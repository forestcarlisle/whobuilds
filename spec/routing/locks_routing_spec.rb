require "spec_helper"

describe LocksController do
  describe "routing" do

    it "recognizes and generates #index" do
      { :get => "/locks" }.should route_to(:controller => "locks", :action => "index")
    end

    it "recognizes and generates #new" do
      { :get => "/locks/new" }.should route_to(:controller => "locks", :action => "new")
    end

    it "recognizes and generates #show" do
      { :get => "/locks/1" }.should route_to(:controller => "locks", :action => "show", :id => "1")
    end

    it "recognizes and generates #edit" do
      { :get => "/locks/1/edit" }.should route_to(:controller => "locks", :action => "edit", :id => "1")
    end

    it "recognizes and generates #create" do
      { :post => "/locks" }.should route_to(:controller => "locks", :action => "create")
    end

    it "recognizes and generates #update" do
      { :put => "/locks/1" }.should route_to(:controller => "locks", :action => "update", :id => "1")
    end

    it "recognizes and generates #destroy" do
      { :delete => "/locks/1" }.should route_to(:controller => "locks", :action => "destroy", :id => "1")
    end

  end
end
