# Read about factories at http://github.com/thoughtbot/factory_girl

Factory.define :person do |f|
  f.sequence(:name) {|n| "Person #{n}" }
  f.sequence(:email) {|n| "person#{n}@test.com" }
  f.password "testtest"
end
