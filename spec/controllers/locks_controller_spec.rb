require 'spec_helper'

describe LocksController do
  # sign_in(Factory.create(:person))

  def mock_lock(stubs={})
    @mock_lock ||= mock_model(Lock, stubs).as_null_object
  end

  describe "GET index" do
    it "assigns all locks as @locks" do
      Lock.stub(:all) { [mock_lock] }
      get :index
      assigns(:locks).should eq([mock_lock])
    end
  end

  describe "GET show" do
    it "assigns the requested lock as @lock" do
      Lock.stub(:find).with("37") { mock_lock }
      get :show, :id => "37"
      assigns(:lock).should be(mock_lock)
    end
  end

  describe "GET new" do
    it "assigns a new lock as @lock" do
      Lock.stub(:new) { mock_lock }
      get :new
      assigns(:lock).should be(mock_lock)
    end
  end

  describe "GET edit" do
    it "assigns the requested lock as @lock" do
      Lock.stub(:find).with("37") { mock_lock }
      get :edit, :id => "37"
      assigns(:lock).should be(mock_lock)
    end
  end

  describe "POST create" do

    describe "with valid params" do
      it "assigns a newly created lock as @lock" do
        Lock.stub(:new).with({'these' => 'params'}) { mock_lock(:save => true) }
        post :create, :lock => {'these' => 'params'}
        assigns(:lock).should be(mock_lock)
      end

      it "redirects to the created lock" do
        Lock.stub(:new) { mock_lock(:save => true) }
        post :create, :lock => {}
        response.should redirect_to(lock_url(mock_lock))
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved lock as @lock" do
        Lock.stub(:new).with({'these' => 'params'}) { mock_lock(:save => false) }
        post :create, :lock => {'these' => 'params'}
        assigns(:lock).should be(mock_lock)
      end

      it "re-renders the 'new' template" do
        Lock.stub(:new) { mock_lock(:save => false) }
        post :create, :lock => {}
        response.should render_template("new")
      end
    end

  end

  describe "PUT update" do

    describe "with valid params" do
      it "updates the requested lock" do
        Lock.should_receive(:find).with("37") { mock_lock }
        mock_lock.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, :id => "37", :lock => {'these' => 'params'}
      end

      it "assigns the requested lock as @lock" do
        Lock.stub(:find) { mock_lock(:update_attributes => true) }
        put :update, :id => "1"
        assigns(:lock).should be(mock_lock)
      end

      it "redirects to the lock" do
        Lock.stub(:find) { mock_lock(:update_attributes => true) }
        put :update, :id => "1"
        response.should redirect_to(lock_url(mock_lock))
      end
    end

    describe "with invalid params" do
      it "assigns the lock as @lock" do
        Lock.stub(:find) { mock_lock(:update_attributes => false) }
        put :update, :id => "1"
        assigns(:lock).should be(mock_lock)
      end

      it "re-renders the 'edit' template" do
        Lock.stub(:find) { mock_lock(:update_attributes => false) }
        put :update, :id => "1"
        response.should render_template("edit")
      end
    end

  end

  describe "DELETE destroy" do
    it "destroys the requested lock" do
      Lock.should_receive(:find).with("37") { mock_lock }
      mock_lock.should_receive(:destroy)
      delete :destroy, :id => "37"
    end

    it "redirects to the locks list" do
      Lock.stub(:find) { mock_lock }
      delete :destroy, :id => "1"
      response.should redirect_to(locks_url)
    end
  end

end
