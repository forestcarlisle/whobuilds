class Lock < ActiveRecord::Base
  belongs_to :person
  belongs_to :machine

  scope :expired, lambda { where("done_at < ?", Time.zone.now) } do
    def clear
      each { |e| e.destroy }
    end
  end
    
  def extend_time(minutes = 30)
    self.done_at = self.done_at.advance(:minutes => minutes)
  end
end
