require 'spec_helper'

describe Person do
  it { should have_many(:locks).dependent(:destroy) }
  it { should have_many(:locked_machines).through(:locks) }

  it { should validate_presence_of(:name) }
  
  subject { Factory.create(:person) }
  it { should validate_uniqueness_of(:name) }
  
  it { should allow_mass_assignment_of(:name) }
  it { should allow_mass_assignment_of(:email) }
  it { should allow_mass_assignment_of(:password) }
  it { should allow_mass_assignment_of(:password_confirmation) }
end
